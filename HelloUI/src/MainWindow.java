import CAALHP.AppAdapter;

import javax.swing.*;

/**
 * Created by rgst on 23-05-2014.
 */
public class MainWindow extends JFrame {
    private AppImplementation appImpl;
    private AppAdapter adapter;

    public MainWindow(){
        setTitle("Simple example");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        appImpl = new AppImplementation();
        adapter = new AppAdapter("localhost", appImpl);
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainWindow ex = new MainWindow();
                HelloWorld hello = new HelloWorld();

                ex.setVisible(true);
                ex.add(hello.panel1);
            }
        });
    }
}
