import CAALHP.Contracts.IAppCAALHPContract;
import CAALHP.Contracts.IAppHostCAALHPContract;

import java.util.AbstractMap;

/**
 * Created by rgst on 23-05-2014.
 */
public class AppImplementation implements IAppCAALHPContract {
    private IAppHostCAALHPContract host;
    private int processId;
    private final String appName = "HelloJavaFX";

    @Override
    public void Initialize(IAppHostCAALHPContract iAppHostCAALHPContract, int pid) {
        host = iAppHostCAALHPContract;
        processId = pid;
    }

    @Override
    public void Show() {

    }

    @Override
    public String GetName() {
        return appName;
    }

    @Override
    public Boolean IsAlive() {
        return true;
    }

    @Override
    public void Notify(AbstractMap.SimpleEntry<String, String> stringStringSimpleEntry) {

    }

    @Override
    public void ShutDown() {

    }
}
